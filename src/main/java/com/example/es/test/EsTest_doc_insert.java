package com.example.es.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;

import java.io.IOException;

public class EsTest_doc_insert {

    public static void main(String[] args) throws IOException {
        //创建ES客户端
        RestHighLevelClient esClient = new RestHighLevelClient(
                RestClient.builder(new HttpHost("localhost",9200,"http"))
        );

        //插入数据
        IndexRequest request = new IndexRequest();
        request.index("user").id("1001");

        User user = new User();
        user.setName("小雨");
        user.setSex("男的");
        user.setAge(28);

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(user);

        request.source(json, XContentType.JSON);

        IndexResponse response = esClient.index(request, RequestOptions.DEFAULT);


        System.out.println("数据查询："+ response.getResult());

        //关闭es客户端
        esClient.close();


    }

}
